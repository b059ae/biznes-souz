<?php

namespace app\components;

use app\models\interfaces\CallerInterface;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 11.08.17
 * Time: 14:16
 */
class Beeline extends Component
{
    private $url = 'http://vn.beeline.ru/com.broadsoft.xsi-actions/v2.0/user/{user}@mpbx.sip.beeline.ru/calls/CallMeNow?address={phone}';

    /**
     * Пользователь из кода виджета для обратного звонка с сайта
     * Находится во вкладке Звонок с сайта на странице https://cloudpbx.beeline.ru/#Профиль
     * Формат MPBX_g_76328_hg_87307
     * @var string
     */
    public $user;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (empty($this->user)){
            throw new InvalidConfigException('Требуется указать beeline.user в настройках');
        }
        $this->url = strtr($this->url,[
            '{user}' => $this->user
        ]);
    }

    /**
     * @inheritdoc
     */
    public function call($phoneNumber)
    {
        $s = curl_init();
        curl_setopt($s, CURLOPT_URL, strtr($this->url,[
            '{phone}' => urlencode('+7'.$phoneNumber)
        ]));
        curl_setopt($s, CURLOPT_POST, true);
        curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
        // Хак для работы с кривым php curl
        curl_setopt($s, CURLOPT_HTTPHEADER, array(
            "Content-Length: 0"
        ));
        curl_exec($s);
    }

}