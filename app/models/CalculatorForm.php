<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CalculatorForm extends Model
{
    public $loan_sum = 5000;
    public $loan_months = 3;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['loan_sum', 'loan_months'], 'required'],
            ['loan_sum', 'integer', 'min' => 5000],
            ['loan_months', 'integer', 'min' => 3],
        ];
    }

    public function attributeLabels(){
        return [
            'loan_sum' => 'Сумма займа',
            'loan_months' => 'Срок займа',
        ];
    }
}
