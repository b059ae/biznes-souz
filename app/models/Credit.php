<?php

namespace app\models;

use Yii;

class Credit extends \yii\db\ActiveRecord
{
    public static function tableName(){
        return 'bs_credits';
    }
    
    
    public function sendToAmoCRM(){
        $amocrm = Yii::$app->amocrm->getClient();
        
        $lead = $amocrm->lead;
        $lead['name'] = 'Заявка на займ от ' . $this->name;
        $lead['responsible_user_id'] = $amocrm->fields['responsible_user_id'];
        $lead->addCustomField($amocrm->fields['loan_sum_cf'], $this->loan_sum);
        $lead->addCustomField($amocrm->fields['loan_total_cf'], $this->getLoanTotal());
        $lead->addCustomField($amocrm->fields['loan_return_date_cf'], $this->getLoanReturnDate());
        $amocrm_lead_id = $lead->apiAdd();
        if(!$amocrm_lead_id){
            return false;
        }
        
        $contact = $amocrm->contact;
        $contact['name'] = $this->name;
        $contact['responsible_user_id'] = $amocrm->fields['responsible_user_id'];
        $contact->addCustomField($amocrm->fields['phone_cf'], [
            [preg_replace('/[^\+\d]/', '', $this->phone), 'WORK'],
        ]);
        $contact->setLinkedLeadsId($amocrm_lead_id);
        $amocrm_contact_id = $contact->apiAdd();
        return !!$amocrm_contact_id;
    }
    
    
    public function callback(){
        // Удаляем спец. символы и первую семерку
        $phone = str_replace(['+', '(', ')', ' ', '-'], [], $this->phone);
        $phone = substr($phone, 1);
        Yii::$app->beeline->call($phone);
    }
    
    public function getLoanReturnDate(){
        return strtotime('+'. $this->loan_months .' months', $this->loan_date);
    }
    
    
    
    
    public function getLoanTotal(){
        $monthly = $this->getLoanMonthly();
        return $monthly * $this->loan_months;
    }
    
    
    
    
    public function getLoanMonthly(){
        $P = 0.84;
        return round(
            ($this->loan_sum * $P / 12)
            / (1 - pow(1 / (1 + $P / 12), $this->loan_months) )
        );
    }
}
