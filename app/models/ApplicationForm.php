<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Credit;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ApplicationForm extends Model
{
    public $name;
    public $phone;
    public $loan_months = 3;
    public $loan_sum = 5000;

    
    public function calculateData() {
        $data = array_filter($this->toArray());
        $data['loan_date'] = time();
        $credit = new Credit($data);
        return [
            'loan_total' => $credit->getLoanTotal(),
            'loan_monthly' => $credit->getLoanMonthly(),
            'loan_return_date' => date('d.m.Y', $credit->getLoanReturnDate()),
        ];
    }
    
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'loan_months', 'loan_sum'], 'required'],
            [['name'], 'string', 'min' => 3],
            [['phone'], 'string', 'min' => 7],
            [['loan_months', 'loan_sum'],  'integer', 'min' => 1],
        ];
    }

    public function attributeLabels(){
        return [
            'name' => 'Имя',
            'phone' => 'Телефон',
        ];
    }
    
    public function createCredit(){
        if(!$this->validate()){
            return null;
        }
        $data = $this->toArray();
        $data['loan_date'] = time();
        $credit = new Credit($data);
        return $credit->save() ? $credit : false;
    }
}
