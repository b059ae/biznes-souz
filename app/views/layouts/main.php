<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\helpers\SiteHelper;

$asset = AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <?php $this->head() ?>
</head>
<body class="<?= SiteHelper::getPageClass(); ?>">
<?php $this->beginBody() ?>

<div class="wrap">

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header pull-left">
                <a class="navbar-brand" href="/">
                    <img src="<?= $asset->baseUrl ?>/img/logo.png">
                </a>
            </div>
            <div class="navbar-header pull-right">
                <p class="navbar-text">
                    <a href="tel:+79001122333">
                        <strong>+7 (900) 11-22-333</strong>
                    </a>
                </p>
            </div>
        </div>
    </nav>

    <div class="container">
        <?= $content ?>
    </div>
</div>
<?php $this->endBody() ?>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
</body>
</html>
<?php $this->endPage() ?>
