<?php

/* @var $this yii\web\View */
/* @var $form_model app\models\ApplicationForm */

/* @var $calculated array */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;

$this->title = 'Заявка на займ';
?>
<div class="site-contact">
    <p class="text-center dark-grey no-margin"><strong><?= Html::encode($this->title) ?></strong></p>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

    <?php else: ?>


        <div class="row">
            <div class="col-xs-12">

                <?php $form = ActiveForm::begin([
                    'id' => 'application-form',
                ]); ?>

                <?= $form
                    ->field($form_model, 'name', [
                        'labelOptions' => ['class' => 'cyan']
                    ])
                    ->textInput(['autofocus' => true]) ?>
                <?= $form
                    ->field($form_model, 'phone', [
                        'labelOptions' => ['class' => 'cyan']
                    ])
                    ->textInput()
                    ->widget(MaskedInput::className(), [
                        'mask' => '+7(999) 999-99-99'
                    ]) ?>
                <?= $form->field($form_model, 'loan_sum')->hiddenInput()->label(false) ?>
                <?= $form->field($form_model, 'loan_months')->hiddenInput()->label(false) ?>

                <table class="table table-condensed table-application">
                    <tr>
                        <td class="text-left grey">Сумма займа, руб.</td>
                        <td class="text-right cyan font-regular"><?= $form_model->loan_sum; ?></td>
                    </tr>
                    <tr>
                        <td class="text-left grey">Срок займа, мес.</td>
                        <td class="text-right cyan font-regular"><?= $form_model->loan_months; ?></td>
                    </tr>
                    <tr>
                        <td class="text-left grey">Сумма возврата, руб.</td>
                        <td class="text-right cyan font-regular"><?= $calculated['loan_total']; ?></td>
                    </tr>
                    <tr>
                        <td class="text-left grey">Ежемесячная выплата, руб.</td>
                        <td class="text-right cyan font-regular"><?= $calculated['loan_monthly']; ?></td>
                    </tr>
                    <tr>
                        <td class="text-left grey">Дата возврата</td>
                        <td class="text-right dark-grey font-regular"><?= $calculated['loan_return_date']; ?></td>
                    </tr>
                </table>

                <?= Html::submitButton('Жду звонка!', ['class' => 'btn btn-primary']) ?>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>
</div>
