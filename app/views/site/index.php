<?php

/* @var $this yii\web\View */

/* @var $form_model app\models\CalculatorForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Рассчитать займ';
?>
<div class="site-contact">
    <p class="text-center dark-grey no-margin"><strong><?= Html::encode($this->title) ?></strong></p>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

    <?php else: ?>
        <div class="row">
            <div class="col-xs-12">

                <?php $form = ActiveForm::begin(['id' => 'calculator-form']); ?>


                <?= $form->field($form_model, 'loan_sum', [
                    'template' => "{label} <strong class='grey'>в тыс. руб.</strong>\n{beginWrapper}"
                        . "\n<div id='loan_sum_scale' class='scale'></div>"
                        . "\n{input}\n{error}\n{endWrapper}",
                    'labelOptions' => [ 'class' => 'cyan' ]
                ])->textInput([
                    'id' => 'loan_sum',
                    'type' => 'text',
                    'class' => null,
                ]) ?>


                <?= $form->field($form_model, 'loan_months', [
                    'template' => "{label} <strong class='grey'>в месяцах</strong>\n{beginWrapper}"
                        . "\n<div id='loan_months_scale' class='scale'></div>"
                        . "\n{input}\n{error}\n{endWrapper}",
                    'labelOptions' => [ 'class' => 'cyan' ]
                ])->textInput([
                    'id' => 'loan_months',
                    'type' => 'text',
                    'class' => null,
                ]) ?>

                <p class="grey"><strong><span class="cyan">Ежемесячный платеж</span> в руб.</strong></p>
                <div class="panel panel-default">
                    <div class="panel-body">
                         <strong id="loan_monthly"></strong>
                    </div>
                </div>

                <p class="grey"><strong><span class="cyan">Общая сумма займа</span> в руб.</strong></p>
                <div class="panel panel-default">
                    <div class="panel-body">
                         <strong id="loan_total"></strong>
                    </div>
                </div>

                <?= Html::submitButton('Получить займ', ['class' => 'btn btn-primary']) ?>

                <?php ActiveForm::end(); ?>

            </div>
        </div>


    <?php endif; ?>
</div>