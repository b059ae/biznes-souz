<?php

/* @var $this yii\web\View */

use app\assets\AppAsset;
use yii\helpers\Html;

$asset = AppAsset::register($this);

$this->title = 'Успех!';
?>
<div class="site-success">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="text-center cyan"><?= Html::encode($this->title) ?></h1>
            <p class="text-center dark-grey"><strong>Ваша заявка отправлена</strong></p>
            <p class="text-center grey font-small">
                Наш менеджер свяжется с вами<br/>в кратчайшие сроки
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <img class="img-responsive center-block success-img" src="<?= $asset->baseUrl ?>/img/operator.png">
        </div>
    </div>
</div>
