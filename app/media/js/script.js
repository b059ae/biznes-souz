jQuery(function($){
    
    var page_class = document.body.className;

    /* Calculator with sliders */
    if(page_class === 'site-index'){
        var loan_sum_field = $('#loan_sum');
        var loan_months_field = $('#loan_months');
        var loan_monthly_field = $('#loan_monthly');
        var loan_total_field = $('#loan_total');
        var loan_slider_handler = computeAndShow(loan_sum_field, loan_months_field, loan_monthly_field, loan_total_field);

        loan_sum_field.slider({
            min : 5000,
            max : 35000,
            step : 1000,
            value : loan_sum_field.val()
        }).on('slide change', loan_slider_handler).trigger('change');
        addScale('loan_sum_scale', 5, 35, 5);

        loan_months_field.slider({
            min : 3,
            max : 13,
            step : 1,
            value : loan_months_field.val()
        }).on('slide change', loan_slider_handler).trigger('change');
        addScale('loan_months_scale', 3, 13, 1);
    }
    
});


/**
 * Calculates all loan data
 * @param {number} sum
 * @param {number} months
 * @returns {object}
 */
function compute(sum, months){
    var output = {};
    var P = 0.84;
    output.monthly = ((sum * P / 12) / (1 - Math.pow(1 / (1 + P / 12), months) )).toFixed(2);
    output.total = (output.monthly * months).toFixed(2);
    output.percent = output.total - sum;
    return output;
}


/**
 * Creates function that calculates data and displays results in specified containers
 * @param {object} sum_field
 * @param {object} months_field
 * @param {object} monthly_container
 * @param {object} total_container
 * @returns {Function}
 */
function computeAndShow(sum_field, months_field, monthly_container, total_container){
    return function(){
        var sum = parseFloat(sum_field.val());
        var months = parseFloat(months_field.val());
        var data = compute(sum, months);
        monthly_container.html(data.monthly);
        total_container.html(data.total);
    };
}


/**
 * Creates html-scale for slider
 * @param {string} id
 * @param {number} from
 * @param {number} to
 * @param {number} step
 * @returns {undefined}
 */
function addScale(id, from, to, step){
    var html = [];
    var offset = 100 / (to - from) * step;
    for(var i = from, left_margin = 0; i <= to; i += step, left_margin += offset){
        html.push('<span style="left:' + (left_margin - 1.5) + '%">' + i + '</span>');
    }
    document.getElementById(id).innerHTML = html.join('');
}

