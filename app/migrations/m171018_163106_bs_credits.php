<?php

use yii\db\Migration;

class m171018_163106_bs_credits extends Migration
{
    public function up(){
        $this->createTable('bs_credits', [
            'id' => $this->primaryKey(11),
            'name' => $this->string(255)->notNull(),
            'phone' => $this->string(50)->notNull(),
            'loan_date' => $this->integer(100)->unsigned(),
            'loan_months' => $this->integer(11)->unsigned(),
            'loan_sum' => $this->integer(11)->unsigned(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
    }

    public function down(){
        $this->dropTable('bs_credits');
    }
}
