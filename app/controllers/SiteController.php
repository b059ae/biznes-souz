<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;


use app\models\ApplicationForm;
use app\models\CalculatorForm;


class SiteController extends Controller
{
    
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $form_model = new CalculatorForm();
        if($form_model->load(Yii::$app->request->post()) && $form_model->validate()){
            $request_data = $form_model->toArray();
            $request_data[0] = '/application';
            return $this->redirect($request_data);
        }
        return $this->render('index', [
            'form_model' => $form_model,
        ]);
    }
    
    
    /**
     * Displays application form page.
     *
     * @return string
     */
    public function actionApplication(){
        $form_model = new ApplicationForm();
        if($form_model->load(Yii::$app->request->post()) && $form_model->validate()){
            $credit = $form_model->createCredit();
            $credit->sendToAmoCRM(); // Отправка в AmoCRM
            $credit->callback(); // Автоматический звонок
            return $this->redirect('/success');
        }
        $form_model->load(['ApplicationForm' => Yii::$app->request->get()]);
        $calculated = $form_model->calculateData();
        return $this->render('application', [
            'form_model' => $form_model,
            'calculated' => $calculated,
        ]);
    }
    
    
    /**
     * Displays page as result of succeful submit of application.
     *
     * @return string
     */
    public function actionSuccess(){
        return $this->render('success');
    }
}
