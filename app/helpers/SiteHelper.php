<?php

namespace app\helpers;

use Yii;

class SiteHelper{
    public static function getPageClass(){
        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;
        return $controller . '-' . $action;
    }
}