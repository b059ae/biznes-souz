<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$basePath =  dirname(__DIR__);
$webroot = dirname($basePath);

$config = [
    'id' => 'basic',
    'name' => 'Бизнес-союз',
    'language' => 'ru-RU',
    'runtimePath' => $webroot . '/runtime',
    'vendorPath' => $webroot . '/vendor',
    'basePath' => $basePath,
    'bootstrap' => ['log'],
    'components' => [
        'amocrm' => [
            'class' => 'yii\amocrm\Client',
            'subdomain' => 'businesssouz',
            'login' => 'business.souz@yandex.ru',
            'hash' => 'b4867f7996a31375f1b77866b6b29a1b',
            'fields' => [
                'responsible_user_id' => 16829884,
                'loan_sum_cf' => 63623,
                'loan_total_cf' => 63629,
                'loan_return_date_cf' => 63633,
                'phone_cf' => 55403,
            ],
        ],
        // Обратный звонок Билайн
        'beeline' => [
            'class' => \app\components\Beeline::class,
            'user' => 'MPBX_g_110488_hg_115477',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'B5XSgbnfYn7eC3R_wCL-2fEDUksjKpY_',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'calculator' => 'site/calculator',
                'application' => 'site/application',
                'success' => 'site/success',
                '<controller:\w+>/view/<slug:[\w-]+>' => '<controller>/view',
            ],
        ],
        // Сжатие CSS и JS
        'view' => [
            'class' => '\rmrevin\yii\minify\View',
            'enableMinify' => !YII_DEBUG,
            'concatCss' => true, // concatenate css
            'minifyCss' => true, // minificate css
            'concatJs' => true, // concatenate js
            'minifyJs' => true, // minificate js
            'minifyOutput' => true, // minificate result html page
            'webPath' => '@web', // path alias to web base
            'basePath' => '@webroot', // path alias to web base
            'minifyPath' => '@webroot/minify', // path alias to save minify result
            'jsPosition' => [\yii\web\View::POS_END], // positions of js files to be minified
            'forceCharset' => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
            'expandImports' => true, // whether to change @import on content
            'compressOptions' => ['extra' => true], // options for compress
            /*'excludeFiles' => [
                'jquery.js', // exclude this file from minification
                'app-[^.].js', // you may use regexp
            ],*/
            'excludeBundles' => [
                //  \dev\helloworld\AssetBundle::class, // exclude this bundle from minification
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
